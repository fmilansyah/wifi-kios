<?php

return [
    "debug"     =>  env("MIKROTIK_DEBUG", false), //  Show debug information
    "connected" =>  env("MIKROTIK_CONNECTED", false), //  Connection state
    "port"      =>  env("MIKROTIK_PORT", 8728),  //  Port to connect to (default 8729 for ssl)
    "ssl"       =>  env("MIKROTIK_SSL", false), //  Connect using SSL (must enable api-ssl in IP/Services)
    "timeout"   =>  env("MIKROTIK_TIMEOUT", 3),     //  Connection attempt timeout and data read timeout
    "attempts"  =>  env("MIKROTIK_ATTEMPTS", 5),     //  Connection attempt count
    "delay"     =>  env("MIKROTIK_DELAY", 3),     //  Delay between connection attempts in seconds
    "ip"        =>  env("MIKROTIK_IP"),     //  IP mikrotik
    "user"      =>  env("MIKROTIK_USER"),     //  User mikrotik
    "password"  =>  env("MIKROTIK_PASSWORD"),     //  Password mikrotik
];
