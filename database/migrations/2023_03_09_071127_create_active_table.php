<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actives', function (Blueprint $table) {
            $table->id();
            $table->string('mikrotik_active_id');
            $table->string('server');
            $table->string('user')->nullable();
            $table->string('address')->nullable();
            $table->string('mac_address')->nullable();
            $table->string('login_by')->nullable();
            $table->string('uptime')->nullable();
            $table->string('idle_time')->nullable();
            $table->string('keepalive_timeout')->nullable();
            $table->string('bytes_in')->nullable();
            $table->string('bytes_out')->nullable();
            $table->string('packets_in')->nullable();
            $table->string('packets_out')->nullable();
            $table->string('radius')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actives');
    }
};
