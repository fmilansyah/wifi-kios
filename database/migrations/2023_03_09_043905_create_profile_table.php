<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('mikrotik_id');
            $table->string('name');
            $table->string('idle_timeout')->nullable();
            $table->string('keepalive_timeout')->nullable();
            $table->string('status_autorefresh')->nullable();
            $table->string('shared_users')->nullable();
            $table->string('add_mac_cookie')->nullable();
            $table->string('mac_cookie_timeout')->nullable();
            $table->string('rate_limit')->nullable();
            $table->string('parent_queue')->nullable();
            $table->string('address_list')->nullable();
            $table->text('on_login')->nullable();
            $table->string('transparent_proxy')->nullable();
            $table->float('price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
};
