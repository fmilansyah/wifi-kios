<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mikrotik_users', function (Blueprint $table) {
            $table->id();
            $table->string('mikrotik_user_id');
            $table->string('name');
            $table->string('password')->nullable();
            $table->string('address')->nullable();
            $table->string('mac_address')->nullable();
            $table->string('profile')->nullable();
            $table->string('routes')->nullable();
            $table->string('email')->nullable();
            $table->string('limit_uptime')->nullable();
            $table->timestamp('active_at')->nullable();
            $table->timestamp('active_into')->nullable();
            $table->string('limit_bytes_in')->nullable();
            $table->string('limit_bytes_out')->nullable();
            $table->string('limit_bytes_total')->nullable();
            $table->string('uptime')->nullable();
            $table->string('bytes_in')->nullable();
            $table->string('bytes_out')->nullable();
            $table->string('packets_in')->nullable();
            $table->string('packets_out')->nullable();
            $table->string('dynamic')->nullable();
            $table->string('disabled')->nullable();
            $table->string('comment')->nullable();
            $table->enum('status', ['1', '2', '3', '4'])->default(1)->comment('1=belum digunakan; 2=dalam proses pembayaran; 3=sudah digunakan; 4=trial');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mikrotik_users');
    }
};
