$(document).ready(function () {
    $('#standby-screen').on('click', function () {
        $(this).slideUp();
        $('#package-type').show();
    });

    $('#back-to-type').on('click', function () {
        $('#package-catalog').hide();
        $('#package-type').show();
    });

    $('.package-btn').on('click', function () {
        let isFree = $(this).attr('data-is-free')

        if (isFree == '1') {
            alert('Pesanan Diproses');
        } else {
            $('#package-type').hide();
            $('#package-catalog').show();
        }
    });

    $('.package-item').on('click', function () {
        $('#package-name').text($(this).attr('data-name'));
        $('#package-price').text('Rp. ' + $(this).attr('data-price'));
        $('#package-speed-download').text('Download : ' + $(this).attr('data-speed-download'));
        $('#package-speed-upload').text('Upload : ' + $(this).attr('data-speed-upload'));
        $('#pay').attr('data-id', $(this).attr('data-id'));
        $('#package-catalog').hide();
        $('#payment-method').show();
    });

    $('#change-package').on('click', function () {
        $('#payment-method').hide();
        $('#package-catalog').show();
    });
});