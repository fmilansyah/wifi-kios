<?php

namespace App\Business\Mikrotik;

use App\Api\RouterosAPI;
use App\Models\Active;
use App\Models\MikrotikUser;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Facades\DB;

class ActiveBusiness
{
    public static function sync()
    {
        DB::beginTransaction();
        try {
            $interfaces = RouterosAPI::getData('/ip/hotspot/active/print');

            $allActive = Active::get();

            $ids = [];
            foreach ($interfaces as $data) {
                $data = collect($data);

                $data = $data->map(function ($value, $key) {
                    return [str_replace('-', '_', $key) => $value];
                })->collapse();

                $profile = $allActive->where('mikrotik_active_id', $data['.id'])->first();
                if (!$profile) {
                    $profile = new Active();
                    $profile->mikrotik_active_id = $data['.id'];
                }
                $profile->fill($data->toArray());
                $profile->save();

                $ids[] = $data['.id'];
            }

            $ids = Active::whereNotIn('mikrotik_active_id', $ids)->pluck('id');

            Active::destroy($ids);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            abort(400, $th->getMessage());
        }
    }

    public static function checkActive()
    {
        DB::beginTransaction();
        try {
            $interfaces = RouterosAPI::getData('/ip/hotspot/active/print');

            $allUser = MikrotikUser::whereIn('name', collect($interfaces)->pluck('user'))->get();

            foreach ($allUser as $user) {
                if ($user->limit_uptime === null || $user->active_at !== null) continue;

                $intoFromSecond = CarbonInterval::make($user->limit_uptime)->totalSeconds;

                $user->active_at = Carbon::now()->toDateTimeString();
                $user->active_into = Carbon::parse($user->active_at)->addSeconds($intoFromSecond);
                $user->save();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            abort(400, $th->getMessage());
        }
    }
}
