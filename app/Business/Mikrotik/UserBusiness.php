<?php

namespace App\Business\Mikrotik;

use App\Api\RouterosAPI;
use App\Helpers\FormaterHelper;
use App\Models\MikrotikUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UserBusiness
{
    public static function sync()
    {
        DB::beginTransaction();
        try {
            $interfaces = RouterosAPI::getData('/ip/hotspot/user/print');

            $allUser = MikrotikUser::get();

            $ids = [];
            foreach ($interfaces as $data) {
                $data = collect($data);

                $data = $data->map(function ($value, $key) {
                    return [str_replace('-', '_', $key) => $value];
                })->collapse();

                $user = $allUser->where('mikrotik_user_id', $data['.id'])->first();
                if (!$user) {
                    $user = new MikrotikUser();
                    $user->mikrotik_user_id = $data['.id'];
                    if (isset($data['comment'])) {
                        if ($data['comment'] === "counters and limits for trial users") {
                            $user->status = MikrotikUser::STATUS_TRIAL;
                        }
                    }
                }
                $user->fill($data->toArray());
                $user->save();

                $ids[] = $data['.id'];
            }

            $ids = MikrotikUser::whereNotIn('mikrotik_user_id', $ids)->pluck('id');

            MikrotikUser::destroy($ids);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            abort(400, $th->getMessage());
        }
    }

    public static function deleteExpired()
    {
        DB::beginTransaction();
        try {
            $currentAt = Carbon::now()->toDateTimeString();
            $datas = MikrotikUser::where('active_into', '<=', $currentAt)->cursor();

            $ids = [];
            foreach ($datas as $data) {
                RouterosAPI::setData('/ip/hotspot/user/remove', ['.id' => $data->mikrotik_user_id]);
                $ids[] = $data->id;
            }

            MikrotikUser::destroy($ids);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            abort(400, $th->getMessage());
        }
    }
}
