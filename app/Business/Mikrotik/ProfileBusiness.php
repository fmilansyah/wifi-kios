<?php

namespace App\Business\Mikrotik;

use App\Api\RouterosAPI;
use App\Models\Profile;
use Illuminate\Support\Facades\DB;

class ProfileBusiness
{
    public static function sync()
    {
        DB::beginTransaction();
        try {
            $interfaces = RouterosAPI::getData('/ip/hotspot/user/profile/print');

            $allProfile = Profile::get();

            $ids = [];
            foreach ($interfaces as $data) {
                $data = collect($data);

                $data = $data->map(function ($value, $key) {
                    return [str_replace('-', '_', $key) => $value];
                })->collapse();

                $profile = $allProfile->where('mikrotik_id', $data['.id'])->first();
                if (!$profile) {
                    $profile = new Profile();
                    $profile->mikrotik_id = $data['.id'];
                }
                $profile->fill($data->toArray());
                $profile->price = self::getPrice($profile->on_login);
                $profile->save();

                $ids[] = $data['.id'];
            }

            $ids = Profile::whereNotIn('mikrotik_id', $ids)->pluck('id');

            Profile::destroy($ids);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            abort(400, $th->getMessage());
        }
    }

    public static function getPrice($text)
    {
        $data = [];
        $price = 0;
        preg_match("/^[^;]*/", $text, $data);
        if (isset($data[0])) {
            $data = \str_replace([':put ', '(', ')', '"',], '', $data[0]);
            $data = \explode(',', $data);
            if (isset($data[2])) {
                $price = (int) $data[2];
            }
        }

        return $price;
    }
}
