<?php

namespace App\Console\Commands;

use App\Api\RouterosAPI;
use App\Jobs\MikrotikActiveUpdateJob;
use App\Jobs\MikrotikDeleteUserExpiredJob;
use App\Jobs\SyncMikrotikJob;
use App\Models\Profile;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MikrotikDeleteUserExpiredCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mikrotik:delete-user-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete if user expired';

    /**
     * Execute the console command.
     *
     * @return intus
     */
    public function handle()
    {
        dispatch(new MikrotikDeleteUserExpiredJob());

        echo "Success Throw To Job";
    }
}
