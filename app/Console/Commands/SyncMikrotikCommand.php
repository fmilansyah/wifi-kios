<?php

namespace App\Console\Commands;

use App\Api\RouterosAPI;
use App\Jobs\SyncMikrotikJob;
use App\Jobs\SyncMikrotikUserJob;
use App\Models\Profile;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncMikrotikCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mikrotik:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync User Profiles, Users, Activitys from mikrotik';

    /**
     * Execute the console command.
     *
     * @return intus
     */
    public function handle()
    {
        dispatch(new SyncMikrotikJob());
        dispatch(new SyncMikrotikUserJob());

        echo "Success Throw To Job";
    }
}
