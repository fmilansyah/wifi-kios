<?php

namespace App\Console\Commands;

use App\Api\RouterosAPI;
use App\Jobs\MikrotikActiveUpdateJob;
use App\Jobs\SyncMikrotikJob;
use App\Models\Profile;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MikrotikUpdateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mikrotik:update-user-active';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update if user was active';

    /**
     * Execute the console command.
     *
     * @return intus
     */
    public function handle()
    {
        dispatch(new MikrotikActiveUpdateJob());

        echo "Success Throw To Job";
    }
}
