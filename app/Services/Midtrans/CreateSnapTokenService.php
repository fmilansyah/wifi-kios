<?php

namespace App\Services\Midtrans;

use Midtrans\Snap;

class CreateSnapTokenService extends Midtrans
{
    protected $order;

    public function __construct($order)
    {
        parent::__construct();

        $this->order = $order;
    }

    public function getSnapToken()
    {
        $params = [
            'transaction_details' => [
                'order_id' => $this->order->number,
                'gross_amount' => $this->order->total_price,
            ],
            'item_details' => [
                [
                    'id' => 1,
                    'price' => $this->order->total_price,
                    'quantity' => 1,
                    'name' => "Pembayaran {$this->order->name_profile}",
                ],
            ],
            // 'customer_details' => [
            //     'first_name' => 'Martin Mulyo Syahidin',
            //     'email' => 'mulyosyahidin95@gmail.com',
            //     'phone' => '081234567890',
            // ]
        ];

        $snapToken = Snap::getSnapToken($params);

        return $snapToken;
    }
}
