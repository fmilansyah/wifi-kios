<?php

namespace App\Jobs;

use App\Business\Mikrotik\ActiveBusiness;
use App\Business\Mikrotik\GetAllProfileBusiness;
use App\Business\Mikrotik\ProfileBusiness;
use App\Business\Mikrotik\UserBusiness;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncMikrotikJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ProfileBusiness::sync();
        // UserBusiness::sync();
        // ActiveBusiness::sync();
    }
}
