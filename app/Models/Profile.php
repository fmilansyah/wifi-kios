<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'mikrotik_id',
        'name',
        'idle_timeout',
        'keepalive_timeout',
        'status_autorefresh',
        'shared_users',
        'add_mac_cookie',
        'mac_cookie_timeout',
        'rate_limit',
        'parent_queue',
        'address_list',
        'on_login',
        'transparent_proxy',
        'price',
    ];
}
