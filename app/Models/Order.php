<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    const PAYMENT_STATUS_PANDING = '1';
    const PAYMENT_STATUS_SUCCESS = '2';
    const PAYMENT_STATUS_CANCEL = '3';

    protected $fillable = [
        'number',
        'name_profile',
        'mikrotik_user_id',
        'total_price',
        'payment_status',
        'snap_token',
    ];
}
