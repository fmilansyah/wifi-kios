<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Active extends Model
{
    use HasFactory;

    protected $fillable = [
        'mikrotik_active_id',
        'server',
        'user',
        'address',
        'mac_address',
        'login_by',
        'uptime',
        'idle_time',
        'keepalive_timeout',
        'bytes_in',
        'bytes_out',
        'packets_in',
        'packets_out',
        'radius',
    ];
}
