<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MikrotikUser extends Model
{
    use HasFactory;

    const STATUS_NOT_USE = '1';
    const STATUS_WAITING_PAYMENT = '2';
    const STATUS_PAID = '3';
    const STATUS_TRIAL = '4';

    protected $fillable = [
        'mikrotik_user_id',
        'name',
        'password',
        'address',
        'mac_address',
        'profile',
        'routes',
        'email',
        'limit_uptime',
        'active_at',
        'active_into',
        'limit_bytes_in',
        'limit_bytes_out',
        'limit_bytes_total',
        'uptime',
        'bytes_in',
        'bytes_out',
        'packets_in',
        'packets_out',
        'dynamic',
        'disabled',
        'comment',
        'status',
    ];
}
