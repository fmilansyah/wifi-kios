<?php

namespace App\Helpers;

use App\Api\RouterosAPI;
use App\Models\Active;
use Carbon\CarbonInterval;
use Illuminate\Support\Facades\DB;

class FormaterHelper
{
    public static function stringToTime($string)
    {
        if ($string === null || $string === '') return null;

        $data = CarbonInterval::make($string);

        $h = str_pad($data->h, 2, 0, STR_PAD_LEFT);
        $i = str_pad($data->i, 2, 0, STR_PAD_LEFT);
        $s = str_pad($data->s, 2, 0, STR_PAD_LEFT);

        return "$h:$i:$s";
    }
}
