<?php

namespace App\Http\Controllers;

use App\Api\RouterosAPI;
use App\Helpers\FormaterHelper;
use App\Models\MikrotikUser;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use App\Models\Profile;

class HomeController extends Controller
{
    public function index()
    {
        $profiles = MikrotikUser::where('status', MikrotikUser::STATUS_NOT_USE)
            ->select('profile')
            ->whereNotNull('profile')
            ->groupBy('profile')
            ->pluck('profile');

        $models = Profile::whereIn('name', $profiles)
            ->where('price', '>', 0)
            ->get();
        return view('welcome', compact('models'));
    }
}
