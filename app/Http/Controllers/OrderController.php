<?php

namespace App\Http\Controllers;

use App\Models\MikrotikUser;
use App\Models\Order;
use App\Models\Profile;
use App\Services\Midtrans\CallbackService;
use App\Services\Midtrans\CreateSnapTokenService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;

        $model = Profile::where('id', $id)->first();

        DB::beginTransaction();
        try {
            if (!$model) throw new Exception("Profile Not Found", 404);

            $modelUser = MikrotikUser::where('profile', $model->name)
                ->where('status', MikrotikUser::STATUS_NOT_USE)
                ->first();

            if (!$modelUser) throw new Exception("Quota Out Of Stock", 404);

            $order = new Order();
            $order->total_price = $model->price ?? 0;
            $order->payment_status = Order::PAYMENT_STATUS_PANDING;
            $order->name_profile = $model->name;
            $order->mikrotik_user_id = $modelUser->id;
            $order->save();
            $order->number = $order->id;
            $order->save();

            $modelUser->status = MikrotikUser::STATUS_WAITING_PAYMENT;
            $modelUser->save();

            $snapToken = $order->snap_token;
            if (is_null($snapToken)) {
                // If snap token is still NULL, generate snap token and save it to database

                $midtrans = new CreateSnapTokenService($order);
                $snapToken = $midtrans->getSnapToken();

                $order->snap_token = $snapToken;
                $order->save();
            }

            DB::commit();
            return response()
                ->json([
                    'code' => 200,
                    'success' => true,
                    'error' => false,
                    'message' => 'Success',
                    'order' => $order,
                    'snapToken' => $snapToken,
                ]);
        } catch (\Throwable $th) {
            DB::rollBack();

            return response()
                ->json([
                    'code' => 400,
                    'success' => false,
                    'error' => true,
                    'message' => $th->getMessage(),
                    'order' => null,
                    'snapToken' => null,
                ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $snapToken = $order->snap_token;
        if (is_null($snapToken)) {
            // If snap token is still NULL, generate snap token and save it to database

            $midtrans = new CreateSnapTokenService($order);
            $snapToken = $midtrans->getSnapToken();

            $order->snap_token = $snapToken;
            $order->save();
        }
        return response()
            ->json([
                'code' => 200,
                'success' => true,
                'error' => false,
                'message' => 'Success',
                'order' => $order,
                'snapToken' => $snapToken,
            ]);
    }

    public function showVoucher(MikrotikUser $user)
    {
        if ($user->status !== MikrotikUser::STATUS_PAID) {
            return response()
                ->json([
                    'code' => 404,
                    'success' => true,
                    'error' => false,
                    'message' => 'Failed User Not Yet Paid',
                    'mikrotik_user' => null,
                ]);
        }

        return response()
            ->json([
                'code' => 200,
                'success' => true,
                'error' => false,
                'message' => 'Success',
                'mikrotik_user' => $user,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
