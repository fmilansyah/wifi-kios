<?php

namespace App\Http\Controllers;

use App\Models\MikrotikUser;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Services\Midtrans\CallbackService;

class PaymentCallbackController extends Controller
{
    public function receive()
    {
        $callback = new CallbackService;

        if ($callback->isSignatureKeyVerified()) {
            $notification = $callback->getNotification();
            $order = $callback->getOrder();

            if ($callback->isSuccess()) {
                Order::where('id', $order->id)->update([
                    'payment_status' => 2,
                ]);

                MikrotikUser::where('id', $order->mikrotik_user_id)->update([
                    'status' => MikrotikUser::STATUS_PAID,
                ]);
            }

            if ($callback->isExpire()) {
                Order::where('id', $order->id)->update([
                    'payment_status' => 3,
                ]);

                MikrotikUser::where('id', $order->mikrotik_user_id)->update([
                    'status' => MikrotikUser::STATUS_NOT_USE,
                ]);
            }

            if ($callback->isCancelled()) {
                Order::where('id', $order->id)->update([
                    'payment_status' => 4,
                ]);

                MikrotikUser::where('id', $order->mikrotik_user_id)->update([
                    'status' => MikrotikUser::STATUS_NOT_USE,
                ]);
            }

            return response()
                ->json([
                    'success' => true,
                    'message' => 'Notifikasi berhasil diproses',
                ]);
        } else {
            return response()
                ->json([
                    'error' => true,
                    'message' => 'Signature key tidak terverifikasi',
                ], 403);
        }
    }
}
