<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wi-Fi Kios</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <link href="/css/custom.css" rel="stylesheet">
</head>

<body>
    <div id="standby-screen" class="standby-screen">
        <div class="image">&nbsp;</div>
        <div class="touch-screen">
            <div class="text">
                SENTUH LAYAR UNTUK MULAI
            </div>
        </div>
    </div>
    <div id="package-type" class="package-type" style="display: none;">
        <div class="package-container">
            <div class="logo">
                <img src="/images/icon-light.png" />
            </div>
            <div class="package-list">
                <div class="package-btn" data-is-free="0">
                    <div class="package-image">
                        <img src="/images/buy-package.png" />
                    </div>
                    Beli Paket
                </div>
                <div class="package-btn" data-is-free="1">
                    <div class="package-image">
                        <img src="/images/free.png" />
                    </div>
                    Dapatkan Gratis
                </div>
            </div>
        </div>
    </div>
    <div id="package-catalog" class="package-catalog" style="display: none;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-1 logo">
                    <img src="/images/icon-light.png" />
                </div>
                <div class="col-md-9 app-title">
                    <h1 style="margin-top: 15px">{{ config('app.name') }} </h1>
                </div>
                <div class="col-md-2" style="text-align: right;">
                    <button style="margin-top: 15px" type="button" id="back-to-type" class="btn btn-light btn-lg"><i
                            class="bi bi-chevron-left"></i> Kembali</button>
                </div>
                <div class="col-md-12">
                    <hr />
                </div>
                @foreach($models as $model)
                @php
                $speed = ['-', '-'];
                $price = number_format($model->price,2);
                @endphp
                @if ($model->rate_limit)
                @php $speed = explode('/', $model->rate_limit); @endphp
                @endif
                <div class="col-md-4">
                    <div class="package-item" data-id="{{ $model->id }}" data-name="{{ $model->name }}"
                        data-price="{{ $price }}" data-speed-download="{{ $speed[0] }}"
                        data-speed-upload="{{ $speed[1] }}">
                        <div class="container">
                            <div class="row">
                                <div class="col-3">
                                    <div class="icon">
                                        <img src="/images/wifi.png" />
                                    </div>
                                </div>
                                <div class="col-9">
                                    <h3>{{ $model->name }}</h3>
                                    <div class="price">Rp. {{ $price }}</div>
                                    <div class="speed">
                                        <div class="row">
                                            <div class="col-6">Download : {{ $speed[0] }}</div>
                                            <div class="col-6">Upload : {{ $speed[1] }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div id="payment-method" class="payment-method" style="display: none;">
        <div class="payment-container">
            <div class="logo">
                <img src="/images/icon-light.png" />
            </div>
            <div class="title">
                <h3>Pembayaran</h3>
            </div>
            <div class="row payment">
                <div class="col-12">
                    <div class="package-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-3">
                                    <div class="icon">
                                        <img src="/images/wifi.png" />
                                    </div>
                                </div>
                                <div class="col-9">
                                    <h3 id="package-name">Paket</h3>
                                    <div id="package-price" class="price">Rp. 0</div>
                                    <div class="speed">
                                        <div class="row">
                                            <div id="package-speed-download" class="col-6">Download : -</div>
                                            <div id="package-speed-upload" class="col-6">Upload : -</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <button style="width: 100%;" type="button" class="btn btn-light btn-lg" id="change-package"><i
                            class="bi bi-chevron-left"></i> Ganti Paket</button>
                </div>
                <div class="col-6">
                    <button style="width: 100%;" type="button" class="btn btn-light btn-lg" id="pay"><i
                            class="bi bi-credit-card"></i> Bayar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="voucher" class="voucher" style="display: none;">
        <div class="voucher-container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4">
                        <div class="logo">
                            <img src="/images/icon-light.png" />
                        </div>
                        <h3 class="title">{{ config('app.name') }}</h3>
                    </div>
                    <div class="col-8">
                        <h3 id="voucher-name">Paket</h3>
                        <div class="voucher-data">
                            Username
                            <div id="voucher-username">XXXXXXXXX</div>
                            Password
                            <div id="voucher-password">XXXXXXXXX</div>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
    </script>
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ config('midtrans.client_key') }}">
    </script>
    <script src="/js/custom.js"></script>

    <script>
        $('#pay').on('click', function () {
            $(this).attr('disabled', '');
            let id = $(this).attr('data-id');
            let request = $.ajax({
                url: '{{ route("orders.store") }}',
                type: 'POST',
                data: { id },
                dataType: 'json',
                success: function(data) {
                    snap.pay(data.snapToken, {
                        // Optional
                        onSuccess: function(result) {
                            /* You may add your own js here, this is just example */
                            // document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                            getVoucher(data);
                        },
                        // Optional
                        onPending: function(result) {
                            /* You may add your own js here, this is just example */
                            // document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                            $('#pay').removeAttr('disabled');
                        },
                        // Optional
                        onError: function(result) {
                            /* You may add your own js here, this is just example */
                            // document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                            $('#pay').removeAttr('disabled');
                        }
                    });
                },
                error: function(xhr) {
                    let err = eval("(" + xhr.responseText + ")");
                    alert(err.message);
                    $('#pay').removeAttr('disabled');
                },
            });
        });

        function getVoucher(data) {
            let request = $.ajax({
                url: '/orders/voucher/'+ data.order.mikrotik_user_id,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    if (data.mikrotik_user === null) {
                        alert(data.message);
                        $('#pay').removeAttr('disabled');
                    } else {
                        $('#voucher-name').text(data.mikrotik_user.profile);
                        $('#voucher-username').text(data.mikrotik_user.name);
                        $('#voucher-password').text(data.mikrotik_user.password);
                        $('#payment-method').hide();
                        $('#voucher').show();;
                    }
                },
                error: function(xhr) {
                    let err = eval("(" + xhr.responseText + ")");
                    alert(err.message);
                    $('#pay').removeAttr('disabled');
                },
            });
        }
    </script>
</body>

</html>
